import React from 'react';
import {
  useInput, useBoolean, useNumber, useWindowSize,
} from 'react-hanger';
import {
  Button, Modal, Input, Checkbox,
} from 'antd';
import './styles.scss';
import _ from 'lodash';

const reactHangerComponent = () => {
  const inputText = useInput('');
  const checkBox = useInput(false);
  console.log('=> checkBox', checkBox.eventBind.value);
  const isShowModal = useBoolean(false);
  const limitedNumber = useNumber(3, { lowerLimit: 0, upperLimit: 5 });
  const counter = useNumber(0);
  const { width, height } = useWindowSize();
  const rotatingNumber = useNumber(0, {
    lowerLimit: 0,
    upperLimit: 4,
    loop: true,
  });
  return (
    <div className="container-react-hanger">
      <Modal title="Basic Modal" visible={isShowModal.value} onOk={isShowModal.toggle} onCancel={isShowModal.toggle}>
        <p>Some contents...</p>
        <p>Some contents...</p>
        <p>Some contents...</p>
      </Modal>
      <div className="react-hanger">
        <div className="use-modal">
          <h2>useBoolean</h2>
          <Button className="btn-modal" type="primary" onClick={isShowModal.toggle}> Show Modal </Button>
          {/* <Button className="btn-modal" onClick={todos.clear}> clear todos </Button> */}
        </div>
        <div className="use-number">
          <h2>useNumber</h2>
          <p>Normal</p>
          <div>
            <Button className="btn-number" onClick={() => counter.decrease()}> - </Button>
            <div className="value-use-number">{counter.value}</div>
            <Button className="btn-number" onClick={() => counter.increase()}> + </Button>
          </div>
          <p>Limited Number</p>
          <div>
            <Button className="btn-number" onClick={() => limitedNumber.decrease()}> - </Button>
            <div className="value-use-number">{limitedNumber.value}</div>
            <Button className="btn-number" onClick={() => limitedNumber.increase()}> + </Button>
          </div>
          <p>Rotating Number</p>
          <div>
            <Button className="btn-number" onClick={() => rotatingNumber.decrease()}> - </Button>
            <div className="value-use-number">{rotatingNumber.value}</div>
            <Button className="btn-number" onClick={() => rotatingNumber.increase()}> + </Button>
          </div>
        </div>
        <div className="use-input">
          <h2>useInput</h2>
          <Input placeholder="Basic usage" type="text" value={inputText.value} onChange={inputText.onChange} />
          <Checkbox onChange={checkBox.onChange}>Checkbox</Checkbox>
        </div>
        <div className="use-window-resize">
          <h2>useWindowSize</h2>
          <p>
            Height:
            {' '}
            {height}
          </p>
          <p>
            Width:
            {' '}
            {width}
          </p>
        </div>
      </div>
    </div>
  );
};

export default reactHangerComponent;
