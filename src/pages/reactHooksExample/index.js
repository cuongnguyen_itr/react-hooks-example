import React from 'react';
import UseDebounceComponent from './useDebounceComponent';
import UseHoverComponent from './useHoverComponent';
import ReactHooksLib from './ReactHooksLib/index';
import ReactHookedUp from './ReactHookedUp/index';
import UseLocalStorageComponent from './useLocalStorageComponent';
import UseMediaQueryComponent from './useMediaQueryComponent';
import UseMouseComponent from './useMouseComponent';
// import ReactRouterComponent from './reactRouter';
import ReactHangerComponent from './reactHanger';
import ReactHotketsHooksComponent from './ReactHotkeys';
import UseLazyLoadImageComponent from './useLazyLoadImage';

const ReactHooksExample = () => (
  <div style={{ padding: '1rem' }}>
    {/* <section>
      <h2>Use hotkeys</h2>
      <ReactHotketsHooksComponent />
    </section> */}
    <section>
      <h2>Use lazy load image</h2>
      <UseLazyLoadImageComponent />
    </section>
    {/* <section>
      <h2>Use Hover</h2>
      <UseHoverComponent />
    </section> */}
    {/* <section>
      <h2>Use Debounce</h2>
      <UseDebounceComponent />
    </section> */}
    {/* <div className="mt-5">
      <ReactHooksLib />
    </div>w
    <div className="mt-5">
      <ReactHookedUp />
    </div> */}
    {/* <section>
      <h4>useMouse</h4>
      <UseMouseComponent />
    </section> */}
    {/* <section>
      <h2>Use Media Query</h2>
      <UseMediaQueryComponent />
    </section> */}
    {/* <section>
      <h2>Use Local Storage</h2>
      <UseLocalStorageComponent />
    </section> */}
    {/* <div className="mt-5">
      <h4>React router</h4>
      <ReactRouterComponent />
    </div> */}
    {/* <div className="mt-5">
      <h4>React Hanger</h4>
      <ReactHangerComponent />
    </div> */}
  </div>
);

export default ReactHooksExample;
