import React from 'react';
import { useTimeout } from 'react-hookedup';

const UseTimeoutComponent = () => {
  useTimeout(() => alert('hello world'), 1500);

  return <h1>hello world</h1>;
};

export default UseTimeoutComponent;
