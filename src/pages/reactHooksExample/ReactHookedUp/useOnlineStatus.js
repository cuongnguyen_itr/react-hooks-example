import React from 'react';
import { useOnlineStatus } from 'react-hookedup';

const UseOnlineStatusComponent = () => {
  const { online } = useOnlineStatus();

  return (
    <h1>
      I am
      {' '}
      {online ? 'online' : 'offline'}
    </h1>
  );
};

export default UseOnlineStatusComponent;
