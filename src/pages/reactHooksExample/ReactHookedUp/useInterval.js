import React, { useState } from 'react';
import { useInterval } from 'react-hookedup';

const UseIntervalComponent = () => {
  const [time, setTime] = useState(new Date().toString());

  useInterval(() => setTime(new Date().toString()), 1000);

  return (
    <div>
      <p>useInterval</p>
      <p>{time}</p>
    </div>
  );
};

export default UseIntervalComponent;
