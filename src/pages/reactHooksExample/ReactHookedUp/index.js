import React from 'react';
import UseIntervalComponent from './useInterval';
import UseTimeoutComponent from './useTimeout';
import UseOnlineStatusComponent from './useOnlineStatus';

const ReactHookedUp = () => (
  <>
    <div>
      <UseIntervalComponent />
    </div>
    <div className="mt-5">
      <UseTimeoutComponent />
    </div>
    <div className="mt-5">
      <UseOnlineStatusComponent />
    </div>
  </>
);

export default ReactHookedUp;
