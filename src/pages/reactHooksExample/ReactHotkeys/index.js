import React, { useState } from 'react';
import { useHotkeys } from 'react-hotkeys-hook';

const ReactHotketsHooksComponent = () => {
  const [count, setCount] = useState(0);
  useHotkeys(
    'ctrl+s',
    () => setCount((prevCount) => prevCount + 1),
    {
      // This will allow the browser to show the save page dialog.
      // Setting it to true will prevent that.
      filterPreventDefault: true,
      filter: () => true,
    },
  );

  return (
    <p>
      Pressed
      {' '}
      {count}
      {' '}
      times.
    </p>
  );
};

export default ReactHotketsHooksComponent;
