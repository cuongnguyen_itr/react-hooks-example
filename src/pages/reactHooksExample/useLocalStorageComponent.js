import React from 'react';
import { useLocalStorage } from '../../utils/customHooks';

const UseLocalStorageComponent = () => {
  const [name, setName, removeName] = useLocalStorage('name', null);
  const [age, setAge, removeAge] = useLocalStorage('age', null);
  return (
    <div>
      <div>
        {name}
        -
        {age}
      </div>
      <button onClick={() => setName('Tung')}>Set Name</button>
      <button onClick={() => setAge(23)}>Set Age</button>
      <button onClick={removeName}>Remove Name</button>
      <button onClick={removeAge}>Remove Age</button>
    </div>
  );
};
export default UseLocalStorageComponent;
