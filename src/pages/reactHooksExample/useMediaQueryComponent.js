import React from 'react';
import useMediaQuery from '../../utils/customHooks/useMediaQuery';

const UseMediaQueryComponent = () => {
  const matches = useMediaQuery('(min-width: 768px)');

  return (
    <div>
      {`The view port is ${matches ? 'at least' : 'less than'} 768 pixels wide`}
    </div>
  );
};

export default UseMediaQueryComponent;
