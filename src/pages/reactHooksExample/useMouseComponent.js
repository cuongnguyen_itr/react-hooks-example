import React from 'react';
import { useMouse } from 'rooks';
import './styles.scss';

const UseMouseComponent = () => {
  const info = useMouse();
  // console.log('=> UseMouseComponent ~ info', info);
  return (
    <>
      <div className="use-mouse">
        Hover on screen and check logs
      </div>
    </>
  );
};

export default UseMouseComponent;
