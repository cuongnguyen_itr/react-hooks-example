import React from 'react';
import { useField } from 'react-hooks-lib';

const UseFieldComponent = () => {
  const { value, bind } = useField('text');

  return (
    <div>
      <p>UseField</p>
      <input type="text" {...bind} />
      <p>{value}</p>
    </div>
  );
};

export default UseFieldComponent;
