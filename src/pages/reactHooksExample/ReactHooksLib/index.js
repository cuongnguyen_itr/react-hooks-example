import React from 'react';
import UseCounterComponent from './useCounter';
import UseFieldComponent from './useField';
import UseFieldComponent2 from './useField2';

const ReactHooksLib = () => (
  <>
    <div>
      <UseCounterComponent />
    </div>
    <div className="mt-5">
      <UseFieldComponent />
      <UseFieldComponent2 />
    </div>
  </>
);

export default ReactHooksLib;
