import React from 'react';
import { useField } from 'react-hooks-lib';

const UseFieldComponent2 = () => {
  const { value, bind } = useField('text');

  return (
    <div>
      <select {...bind}>
        <option value="apple">apple</option>
        <option value="orange">orange</option>
      </select>
      <p>{value}</p>
    </div>
  );
};

export default UseFieldComponent2;
