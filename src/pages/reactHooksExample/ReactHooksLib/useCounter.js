import React from 'react';
import { useCounter } from 'react-hooks-lib';

const UseCounterComponent = () => {
  const {
    count, inc, dec, reset,
  } = useCounter(0);
  return (
    <div>
      <p>UseCounter</p>
      <p>{count}</p>
      <button onClick={() => inc(1)}>increment</button>
      <button onClick={() => dec(1)}>decrement</button>
      <button onClick={reset}>reset</button>
    </div>
  );
};

export default UseCounterComponent;
