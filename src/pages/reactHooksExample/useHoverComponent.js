import React from 'react';
import useHover from 'react-use-hover';
import classnames from 'classnames';
import './styles.scss';

const UseHoverComponent = () => {
  const [isHovering, hoverProps] = useHover({
    // mouseEnterDelayMS: 1000,
    // mouseLeaveDelayMS: 1000,
  });

  return (
    <>
      <span {...hoverProps}>
        Hover mew
      </span>
      <div className={classnames('hover-content', isHovering ? '--hovered' : '')}>
        Hello!!!
      </div>
    </>
  );
};

export default UseHoverComponent;
