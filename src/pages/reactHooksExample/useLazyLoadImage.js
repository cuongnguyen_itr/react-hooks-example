import React from 'react';
import useLazyLoadImage from 'react-use-lazy-load-image';
import _ from 'lodash';

const UseLazyLoadImageComponent = () => {
  useLazyLoadImage();

  return (
    <>
      <div>Lots of content that means the image is off screen goes here</div>
      {
        _.map(_.range(100), () => (
          <img src="DATA URL" data-img-src="https://zoipet.com/wp-content/uploads/2020/04/cho-corgi.jpg" alt="My image" />
        ))
      }
    </>
  );
};

export default UseLazyLoadImageComponent;
