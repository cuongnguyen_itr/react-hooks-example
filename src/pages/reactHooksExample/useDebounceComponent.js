import React, { useState } from 'react';
import { useDebounce } from 'use-debounce';
import BottomScrollListener from 'react-bottom-scroll-listener';
import { useUpdateEffect } from '../../utils/customHooks';
import './styles.scss';

const UseDebounceComponent = () => {
  const [text, setText] = useState('Hello');
  const [isLoading, setIsLoading] = useState(false);
  const [isScrolling, setIsScrolling] = useState(false);
  const [isFetching, setIsFetching] = useState(false);

  const [searchValue] = useDebounce(text, 1000);
  const [scrollDebounce] = useDebounce(isScrolling, 1000);

  const useBottomScrollListener = () => {
    setIsScrolling(true);
  };

  useUpdateEffect(() => {
    setIsLoading(true);
    setTimeout(() => {
      setIsLoading(false);
    }, 2000);
  }, [searchValue]);

  useUpdateEffect(() => {
    setIsFetching(true);
    setTimeout(() => {
      setIsFetching(false);
      setIsScrolling(false);
    }, 2000);
  }, [scrollDebounce]);

  return (
    <div>
      <div>
        <input
          defaultValue="Hello"
          onChange={(e) => {
            setText(e.target.value);
          }}
        />
        {
          isLoading && (
            <span>Loading...</span>
          )
        }
      </div>

      <BottomScrollListener onBottom={useBottomScrollListener}>
        {
          (scrollRef) => (
            <>
              <div ref={scrollRef} className="scroll-container">
                <div className="scroll-zone" />
              </div>
              {
                isFetching && (
                  <span>Loading...</span>
                )
              }
            </>
          )
        }
      </BottomScrollListener>
    </div>
  );
};

export default UseDebounceComponent;
