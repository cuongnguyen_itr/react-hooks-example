import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import ReactHooksExample from './reactHooksExample';
import 'antd/dist/antd.css';

const Main = () => (
  <main>
    <Router>
      <Switch>
        <Route path="/" name="React hooks example" component={ReactHooksExample} />
      </Switch>
    </Router>
  </main>
);

export default Main;
