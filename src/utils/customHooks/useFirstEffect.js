import { useRef, useEffect } from 'react';
import _ from 'lodash';

export default function useFirstEffect(effect, dependencies = [], cleanup) {
  const isInitialMount = useRef(true);
  useEffect(() => {
    if (isInitialMount.current) {
      isInitialMount.current = false;
      effect();
    }
    return cleanup;
  }, dependencies);
}
