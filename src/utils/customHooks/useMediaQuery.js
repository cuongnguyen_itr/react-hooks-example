import { useState, useEffect } from 'react';

export default function useMediaQuery(query) {
  const matchMedia = window.matchMedia(query);
  const [matches, setMatches] = useState(matchMedia.matches);

  const handleChange = () => setMatches(matchMedia.matches);

  useEffect(() => {
    matchMedia.addEventListener('change', handleChange);
    return () => {
      matchMedia.removeEventListener('change', handleChange);
    };
  }, [query]);

  return matches;
}
